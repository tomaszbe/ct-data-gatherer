// Lokalizacje i ich identyfikatory w CarTraveler

module.exports = [
  { name: 'Balice', id: 3197 },
  { name: 'Pyrzowice', id: 4220 },
  { name: 'Okęcie', id: 3204 },
  { name: 'Modlin', id: 150965 },
  { name: 'Gdańsk', id: 3195 },
  { name: 'Wrocław', id: 3205 },
  { name: 'Poznań', id: 3200 },
  { name: 'Szczecin', id: 7016 },
  { name: 'Rzeszów', id: 48712 },
  { name: 'Lublin', id: 153589 },
]