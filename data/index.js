module.exports = {
  locations: require('./locations'),
  dayAheads: require('./dayAheads'),
  durations: require('./durations')
}