const ct = require('.')
const fs = require('fs')
const path = require('path')
const { locations, dayAheads, durations } = require('./data')
const { todayString } = require('./src/utils')
const progress = require('cli-progress')

;(async () => {
  for (let locationIndex in locations) {
    const location = locations[locationIndex]

    let promises = []

    let errorCount = 0

    let resultsCount = 0

    let requestCount = 0

    // console.log('Oddział ' + location.name)
    const bar = new progress.Bar(
      {
        // prettier-ignore
        format: `${location.name} {bar} {percentage}% | ETA: {eta}s | {value}/{total} | Errors: {errors}`
      },
      progress.Presets.shades_classic
    )

    dayAheads.forEach(daysAhead => {
      durations.forEach(duration => {
        let requestNumber = ++requestCount
        promises.push(
          (async () => {
            try {
              let response = await ct.request(daysAhead, duration, location.id)

              // console.log(++resultsCount + '/' + requestCount)
              bar.increment()

              let result = response
                .map(row =>
                  [
                    requestNumber,
                    location.name,
                    daysAhead,
                    duration,
                    ...row
                  ].join(';')
                )
                .join('\n')
              return result
            } catch (error) {
              errorCount++
              resultsCount++
              // console.error('Request #' + requestNumber + ' failed.')
              bar.update(bar.value, { errors: errorCount })
              return [
                requestNumber,
                location.name,
                daysAhead,
                duration,
                'Request failed.',
                ...Array(4)
              ].join(';')
            }
          })()
        )
      })
    })

    bar.start(requestCount, 0, { errors: 0 })

    let results = await Promise.all(promises)

    let out =
      '#;Location;Days Ahead;Duration;Vendor;Acriss;Size;Model;TotalCharge\n'

    out += results.join('\n')

    // console.log('Liczba zapytań:', requestCount)
    // console.log('Liczba błędów:', errorCount)

    const dir = path.join(require('./outPath'), todayString())

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir)
    }

    fs.writeFileSync(path.join(dir, location.name) + '.csv', out)

    bar.stop()
    console.log()
  }
})()
