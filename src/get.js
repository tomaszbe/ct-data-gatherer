const { create } = require('axios')

const http = create({
  baseURL: 'https://otageo.cartrawler.com/cartrawlerota'
})

const get = async msg => {
  const response = await http.get('json', {
    params: {
      msg,
      type: 'OTA_VehAvailRateRQ'
    }
  })
  return response.data
}

module.exports = get
