module.exports = {
  "@Target": "Production",
  "@PrimaryLangID": "pl",
  "POS": {
    "Source": [
      {
        "@ERSP_UserID": "MP",
        "@ISOCurrency": "PLN",
        "@ISOCountry": "PL",
        "RequestorID": {
          "@Type": "16",
          "@ID": "643826",
          "@ID_Context": "CARTRAWLER"
        }
      },
      {
        "RequestorID": {
          "@Type": "16",
          "@ID": "921541591969329",
          "@ID_Context": "CUSTOMERID"
        }
      },
      {
        "RequestorID": {
          "@Type": "16",
          "@ID": "411542104190155",
          "@ID_Context": "ENGINELOADID"
        }
      },
      {
        "RequestorID": {
          "@Type": "16",
          "@ID": "643826",
          "@ID_Context": "PRIORID"
        }
      },
      {
        "RequestorID": {
          "@Type": "16",
          "@ID": "CTABE_V5:5.82.1",
          "@Instance": "RBpuZKasLW2XX+ouNDUZZJGhqxk=",
          "@ID_Context": "VERSION"
        }
      },
      {
        "RequestorID": {
          "@Type": "16",
          "@ID": "3",
          "@ID_Context": "BROWSERTYPE"
        }
      }
    ]
  },
  "@xmlns": "http://www.opentravel.org/OTA/2003/05",
  "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
  "@Version": "1.005",
  "VehAvailRQCore": {
    "@Status": "Available",
    "VehRentalCore": {
      "@PickUpDateTime": "2018-11-14T15:00:00",
      "@ReturnDateTime": "2018-11-15T15:00:00",
      "PickUpLocation": {
        "@CodeContext": "CARTRAWLER",
        "@LocationCode": 3197
      },
      "ReturnLocation": {
        "@CodeContext": "CARTRAWLER",
        "@LocationCode": 3197
      }
    },
    "VehPrefs": {
      "VehPref": {
        "VehClass": {
          "@Size": "0"
        }
      }
    },
    "DriverType": {
      "@Age": 30
    }
  },
  "VehAvailRQInfo": {
    "Customer": {
      "Primary": {
        "CitizenCountryName": {
          "@Code": "PL"
        },
        "CustLoyalty": []
      }
    },
    "TPA_Extensions": {
      "showBaseCost": true,
      "ConsumerIP": "89.25.219.29",
      "GeoRadius": 5,
      "Window": {
        "@name": "Ryanair%20wynajem%20samochod%F3w",
        "@engine": "CTABE-V5.0",
        "@svn": "5.82.1-02",
        "@region": "pl",
        "@device": "DESKTOPWEB",
        "UserAgent": "Mozilla/5.0+(Windows+NT+10.0;+Win64;+x64)+AppleWebKit/537.36+(KHTML,+like+Gecko)+Chrome/70.0.3538.77+Safari/537.36",
        "BrowserName": "chrome",
        "BrowserVersion": "70",
        "URL": "https://car-hire.ryanair.com/pl-pl/book?age=30&clientID=643826&ct=MP&countryID=PL&curr=PLN&elID=411542104190155&pickupCountryCode=PL&pickupDateTime=2018-11-14T15%3A00&residenceID=PL&returnCountryCode=PL&returnDateTime=2018-11-15T15%3A00&pickupID=3197&pickupName=Krak%C3%B3w%20%E2%80%93%20lotnisko%20im.%20Jana%20Paw%C5%82a%20II%2C%20Balice&returnID=3197&returnName=Krak%C3%B3w%20%E2%80%93%20lotnisko%20im.%20Jana%20Paw%C5%82a%20II%2C%20Balice"
      },
      "Tracking": {
        "SessionID": "411542104190155",
        "CustomerID": "921541591969329",
        "EngineLoadID": "411542104190155"
      },
      "RefID": {}
    }
  }
}
