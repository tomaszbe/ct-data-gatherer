const get = require('./get')
const createMessage = require('./createMessage')

const parseResponse = response => {
  const result = []

  if (
    !response.VehAvailRSCore ||
    !response.VehAvailRSCore.VehVendorAvails ||
    !response.VehAvailRSCore.VehVendorAvails.forEach
  ) {
    return [['Brak wyników', ...Array(4)]]
  }

  response.VehAvailRSCore.VehVendorAvails.forEach(vendor => {
    vendor.VehAvails.forEach(({ VehAvailCore: vehicle }) => {
      result.push([
        vendor.Vendor['@CompanyShortName'],
        vehicle.Vehicle['@Code'],
        vehicle.Vehicle.VehClass['@Size'],
        vehicle.Vehicle.VehMakeModel['@Name'],
        vehicle.TotalCharge['@RateTotalAmount']
      ])
    })
  })

  return result
}

exports.request = async (daysFromNow, duration, location) => {
  const msg = createMessage(daysFromNow, duration, location)
  const response = await get(msg)
  return parseResponse(response)
}
