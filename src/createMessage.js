const emptyMessage = require('./emptyMessage')
const { daysFromNow, clone, fakeIp } = require('./utils')

const createMessage = (from, duration, location) => {
  /** @type {typeof emptyMessage} */
  const message = clone(emptyMessage)
  const coreRef = message.VehAvailRQCore.VehRentalCore
  coreRef['@PickUpDateTime'] = daysFromNow(from)
  coreRef['@ReturnDateTime'] = daysFromNow(
    (from ? from : 1) + (duration ? duration : 1)
  )
  coreRef.PickUpLocation['@LocationCode'] = location ? location : 3197
  coreRef.ReturnLocation['@LocationCode'] = location ? location : 3197
  message.VehAvailRQInfo.TPA_Extensions.ConsumerIP = fakeIp()
  return message
}

module.exports = createMessage
