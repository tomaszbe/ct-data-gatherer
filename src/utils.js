const faker = require('faker')
const moment = require('moment')

exports.clone = obj => JSON.parse(JSON.stringify(obj))

exports.daysFromNow = days => moment({ hour: 12 }).add(days ? days : 1, 'd')

exports.fakeIp = () => faker.internet.ip()

exports.todayString = () => moment().format('Y-MM-DD')
